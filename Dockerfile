#http://www.rtl-sdr.com/rtl-sdr-tutorial-analyzing-gsm-with-airprobe-and-wireshark/

FROM sysrun/gnuradio-sdr:latest

MAINTAINER Frederik Granna

WORKDIR /opt

RUN apt-get update && \
    apt-get install -y liblog4cpp5-dev python-scipy \
    libpcsclite-dev libtool libtalloc-dev shtool autoconf automake wireshark && \
    apt-get clean && apt-get autoremove && \
    rm -rf /var/lib/apt/lists/*

RUN git clone git://git.osmocom.org/libosmocore.git && \
    cd libosmocore && git clean -dfx  && \
    autoreconf -i && ./configure && make && make install && ldconfig && \
    rm -rf /opt/libosmocore

ENV gr_gsm_commit_id 6965a3369978eeeee36847f8f18c30927107cce2


RUN git clone https://github.com/ptrkrysik/gr-gsm.git && \
    cd gr-gsm && git checkout $gr_gsm_commit_id && \
    mkdir build && cd build && cmake ../ && make -j4 && make install && \
    rm -rf /opt/gr-gsm
    
